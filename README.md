# IntelRealSenseBarCodes

## Установка зависимостей

1. Библиотека для работы с камерой Intel Real Sense

https://github.com/IntelRealSense/librealsense/releases/tag/v2.53.1

* Скачиваем релиз и распаковываем
* Открываем терминал в директории проекта
* Выполняем команды

```
mkdir build
cd build
cmake ..
make
sudo make install
```

2. Установка OpenCV

Устанавливаем из пакетного менеджера

Пример для Ubuntu

```
sudo apt update
sudo apt install libopencv-dev
```

3. Установка ZXing

https://github.com/zxing-cpp/zxing-cpp/releases/tag/v2.0.0

* Скачиваем релиз и распаковываем
* Открываем терминал в директории проекта
* Выполняем команды

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
```

## Компиляция и запуск

Подключаем камеру Intel Realsense

### Компиляция
```
mkdir build
cd build
cmake ..
make
```
Или же открываем проект через CLion и нажимаем кнопку сборки

### Запуск

В папке build
```./IntelRealSense-BarCodes```

Или через кнопку ```Run``` в CLion 